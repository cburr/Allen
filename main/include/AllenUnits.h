#pragma once

namespace Allen {
  namespace Units {
    //
    // Data size
    //
    constexpr unsigned kB = 1024;
    constexpr unsigned MB = kB * kB;
    constexpr unsigned GB = MB * kB;
  } // namespace Units
} // namespace Allen
