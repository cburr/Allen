#pragma once

#include <zmq_compat.h>
#include <ZeroMQ/IZeroMQSvc.h>

IZeroMQSvc* makeZmqSvc();
